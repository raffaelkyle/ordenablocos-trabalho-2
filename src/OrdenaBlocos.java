import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;


public class OrdenaBlocos {
	
	
	public static void ordenar (int numRegistros,
			int numRegistrosBloco, String arquivo) throws IOException {
		
		
		BufferedReader input = new BufferedReader(new FileReader(arquivo));
		PrintWriter output = new PrintWriter("output.txt", "ISO-8859-1");
		
		int registrosRestantes = numRegistros % numRegistrosBloco;
		int contador = 0;
		
		
		ArrayList<CEP> listaCeps;
		
		String linha;
		
		
		while(contador < (numRegistros / numRegistrosBloco)) {

			listaCeps = new ArrayList<CEP>();
			
			for (int i = 0; i < numRegistrosBloco; i++) {
				
				if((linha = input.readLine()) != null) {
					
					String logradouro = linha.substring(0, 72).trim();
		            String bairro = linha.substring(72, 144).trim();
		            String cidade = linha.substring(144, 216).trim();
		            String uf = linha.substring(216, 288).trim();
		            String sigla = linha.substring(288, 290).trim();
		            String cep = linha.substring(290, 299).trim();
					
					listaCeps.add(new CEP(logradouro, bairro, cidade, uf,
							sigla, Long.parseLong(cep)));
				}
			}
			
			Collections.sort(listaCeps);
			
			for (int i = 0; i < listaCeps.size(); i++) {
				
				output.write(listaCeps.get(i).toString());
				output.println();
			}
			
			contador++;
		}
		
		if (registrosRestantes == 1) {
			
			listaCeps = new ArrayList<CEP>();
			
			if ((linha = input.readLine()) != null) {
				
				String logradouro = linha.substring(0, 72).trim();
				String bairro = linha.substring(72, 144).trim();
				String cidade = linha.substring(144, 216).trim();
				String uf = linha.substring(216, 288).trim();
				String sigla = linha.substring(288, 290).trim();
				String cep = linha.substring(290, 299).trim();
			
				listaCeps.add(new CEP(logradouro, bairro, cidade, uf,
						sigla, Long.parseLong(cep)));
				
				output.write(listaCeps.get(0).toString());
				output.println();
			}
		}
		else {
			
			listaCeps = new ArrayList<CEP>();
			
			for (int i = 0; i < registrosRestantes; i++) {
				
				if((linha = input.readLine()) != null) {
					
					String logradouro = linha.substring(0, 72).trim();
					String bairro = linha.substring(72, 144).trim();
					String cidade = linha.substring(144, 216).trim();
					String uf = linha.substring(216, 288).trim();
					String sigla = linha.substring(288, 290).trim();
					String cep = linha.substring(290, 299).trim();
				
					listaCeps.add(new CEP(logradouro, bairro, cidade, uf,
							sigla, Long.parseLong(cep)));
				}
			}
			
			Collections.sort(listaCeps);
			
			for (int i = 0; i < listaCeps.size(); i++) {
				
				output.write(listaCeps.get(i).toString());
				output.println();
			}
		}
		
		input.close();
		output.close();
	}
}